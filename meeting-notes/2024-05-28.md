# bootable containers initiative meeting - 2024-05-28

## Attendees

- Timothée Ravier
- Jason Brooks
- Robert Sturla
- Noel Miller
- Paul Whalen
- Joseph Marrero
- Hristo Marinov

## Agenda

- Should we record the meetings? https://gitlab.com/fedora/bootc/tracker/-/issues/10
- Plans & roadmap

## Recording meetings?

- Might need to move away from Google Meet (corporate account) to be able to share the recording and upload them somewhere
- Alternating with Matrix chat meetings so those are recorded "by default"
- Should we make the meeting last 1h? and only have it once a week?
- Action: Jason to ask around how we could record and share meetings recording

## Planning

- What issues are blocking what, what's the priority?
- What's the roadmap?
- Created: https://gitlab.com/fedora/bootc/tracker/-/issues/11
- Action: Noel to reach out to stakeholders to help setting up a roadmap

## Fedora Change: DNF and bootc in Image Mode Fedora variants

- Created an issue to track it: https://gitlab.com/fedora/bootc/tracker/-/issues/12
