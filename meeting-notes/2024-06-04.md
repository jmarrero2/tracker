# bootable containers initiative meeting - 2024-06-04

## Attendees

- Timothée Ravier
- Hristo Marinov
- Jason Brooks
- Paul Whalen
- Colin Walters
- Jonathan Lebon
- Peter Robinson
- Micah Abbott
- Eric Curtin

## Agenda

## Decide who will be writting notes and who will make the meeting notes PR

- walters will do it today

## Socializing in Fedora

- Planning to mention this in devconf.cz talk https://pretalx.com/devconf-cz-2024/talk/AVSUU3/
- Someone made: https://fedoramagazine.org/a-great-journey-towards-fedora-coreos-and-bootc/
- Fedora Changes 
    - https://discussion.fedoraproject.org/t/f41-change-proposal-dnf-and-bootc-in-image-mode-fedora-variants-system-wide/117664
    - https://gitlab.com/fedora/bootc/tracker/-/issues/11
    - https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/message/ITBTRAJE2XORB4GZYY2JW4JNPKVX5A3F/

## Image stability, bootc stability, ecosystem, anaconda changes?

- Anaconda changes:
  - https://github.com/rhinstaller/anaconda/discussions/5197
  - ublue has custom installer tooling
  - anaconda changes/issues (can we get issues filed/linked for these)
        - Created a tracker: https://gitlab.com/fedora/bootc/tracker/-/issues/20

- Support in Kiwi in progress:
    - https://github.com/OSInside/kiwi/issues/38
    - https://github.com/travier/fedora-kinoite/blob/main/fedora-kinoite-live/Containerfile 
    - overlaps with bootc-image-builder
