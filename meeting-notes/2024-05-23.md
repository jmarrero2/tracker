# bootable containers initiative meeting - 2024-05-23

This etherpad is ephemeral.
The Notes will end up in https://gitlab.com/fedora/bootc/tracker

## Attendees

- Timothée Ravier
- Robert Sturla
- Noel Miller
- Jason Brooks
- Liora Milbaum
- Jonathan Lebon
- Paul Whalen
- Micah Abbott
- Hristo Marinov

## Agenda

- Introductions
- CI

Jason: Other related projects
- Noel Miller and Robert Sturla are prepping Universal Blue

### Round of short introductions

- Paul Whalen
    - Working on Fedora IoT
    - tiers one and zero building as part of the Fedora IoT compose
    - zero not yet committed to repo, smaller image
- Micah Abbott
    - Eng Manager at Red Hat; leading RHEL for Edge team, interested in Atomic Desktops, Fedora IoT
- Jason Brooks
    - Open Source Program Office at Red Hat
    - Fedora Initiative approved by the Fedora Council
- Liora Milbaum
    - CI/CD lead for bootc at Red Hat
- Jonathan Lebon
    - CoreOS architect at Red Hat
- Robert Sturla
    - Platform engineer
    - Core contributor to Universal Blue
- Hristo Marinov
    - Systems Administrator
- Noel Miller
    - Ansible Technical Account Manager
    - Working with Universal Blue
- Peter Robinson
    - Feodra IoT & RHEL Edge lead
- Timothée Ravier
    - Fedora Atomic Desktops and Fedora CoreOS
    - CoreOS team at Red Hat

### Update on CI and Renovate

- Konflux: Looking at setting up Konflux in Fedora
    - https://redhat-appstudio.github.io/docs.appstudio.io/Documentation/main/
    - https://konflux-ci.dev/
    - https://github.com/konflux-ci/konflux-ci
- CI for https://gitlab.com/fedora/bootc
- https://gitlab.com/fedora/bootc/tracker/-/issues/1
- Renovate:
    - Open source tool to update components
    - https://github.com/renovatebot/renovate
    - re: bodhi + automated tests - example update where coreos tests ran
        - https://bodhi.fedoraproject.org/updates/FEDORA-2024-92664ae6fe

### Tagging issues in Bootc as discussion topics for the meeting

- @Timothee, do we have tags available in the Gitlab repo?
- Which repo should these fall under?
- [travier] I added some labels in https://gitlab.com/fedora/bootc/tracker/-/labels

### Open Questions

- [miabbott] should we record the meetings for playback later?
- [noel] +1 on this
- [travier] We'll have problems with access to the recording so we'll have to upload it somewhere, etc.
- [travier] Made https://gitlab.com/fedora/bootc/tracker/-/issues/10 for the next meeting
